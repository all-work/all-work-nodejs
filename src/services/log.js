/**
 * @default services/log.js
 * @description This file contains the log services for the application
 * @author Gabriel Vaz <gabrielcvaz@outlook.com>
 * @since  December 30th, 2018
 */
// Importing modules
const fs = require("fs");
const moment = require("moment");

const createLog = message => {
  const date = moment().format("DD[.]MM[.]YYYY");
  const file = `${__dirname}/../../logs/${date}.log`;
  const datetime = moment().format("DD[.]MM[.]YYYY HH[:]mm[:]ss");
  message = `Date: ${datetime} | ${message}\n`;
  fs.appendFileSync(file, message);
};

const startLog = () => {
  createLog("*******************************");
};

// Exporting the functions into the module
module.exports = { startLog, createLog };
