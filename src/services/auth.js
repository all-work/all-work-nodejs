/**
 * @default services/auth.js
 * @description This file contains the authentication services for the application
 * @author Gabriel Vaz <gabrielcvaz@outlook.com>
 */
const jwt = require("jsonwebtoken");

const Config = require("../../config/config");

const authMiddleware = (req, res, next) => {
  const token = req.headers ? req.headers.authorization : "";

  jwt.verify(token, Config.secret, (err, decoded) => {
    if (err) {
      return res.status(403).json({
        message: "Unauthorized"
      });
    }

    req.decoded = decoded;
    return next();
  });
};

// Exporting the functions into the module
module.exports = { authMiddleware };
