/**
 * @default controllers/user.js
 * @author Gabriel Vaz <gabrielcvaz@outlook.com>
 */
const express = require("express");

// Importing models
const userModel = require("../models/user");

// Start router
const router = express.Router();

/**
 * Example
 *
 * @param {example} example
 */
router.post("/login", async (req, res) => {
  let response;

  try {
    response = await userModel.login({ ...req.body });
  } catch (error) {
    return res
      .status(error.status || 500)
      .json({ ...error, status: undefined });
  }

  res.status(response.status || 200).json({ ...response, status: undefined });
});

/**
 * Example
 *
 * @param {example} example
 */
router.post("/cadastro", async (req, res) => {
  let response;

  try {
    response = await userModel.cadastro({ ...req.body });
  } catch (error) {
    return res
      .status(error.status || 500)
      .json({ ...error, status: undefined });
  }

  res.status(response.status || 200).json({ ...response, status: undefined });
});

// Export router
module.exports = app => app.use("/user", router);
