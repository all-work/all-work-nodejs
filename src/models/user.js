/**
 * @default models/user.js
 * @author Gabriel Vaz <gabrielcvaz@outlook.com>
 */
const Stampit = require("@stamp/it");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Models = require("../../models");
const Config = require("../../config/config");
const { isEmpty, validateEmail } = require("../services/utils");

// Export Stampit (Simulates native prototype)
module.exports = Stampit({
  statics: {
    async login(body) {
      if (isEmpty(body.email) || isEmpty(body.password))
        throw {
          message: "Por favor escreva um email e senha válidos",
          status: 400
        };

      const user = await Models.User.findOne({
        where: {
          email: body.email
        }
      });

      if (!user)
        throw {
          message: "Esse usuário não existe",
          status: 401
        };

      if (!bcryptjs.compareSync(body.password, user.password))
        throw {
          message: "Essa senha não é válida",
          status: 401
        };

      user.password = undefined;

      const token = jwt.sign({ id: user.id }, Config.secret, {
        expiresIn: "1 year"
      });

      return {
        message: `Bem vindo ${user.name}`,
        user,
        status: 200,
        token
      };
    },
    async cadastro(body) {
      if (
        isEmpty(body.name) ||
        isEmpty(body.phone) ||
        isEmpty(body.document) ||
        isEmpty(body.DocumentId) ||
        isEmpty(body.email) ||
        !validateEmail(body.email) ||
        isEmpty(body.password)
      )
        throw {
          message: "Por favor escreva todos os campos obrigatórios",
          status: 400
        };

      if (body.password.length < 6)
        throw {
          message: "Sua senha deve possuir ao menos 6 caracteres"
        };

      let user = await Models.User.findOne({
        where: {
          email: body.email
        }
      });

      if (user) {
        throw {
          message: "Já existe um usuário com esse E-mail",
          status: 400
        };
      }

      const password = bcryptjs.hashSync(body.password, bcryptjs.genSaltSync());

      try {
        user = await Models.User.create({
          name: body.name,
          email: body.email,
          phone: body.phone,
          document: body.document,
          DocumentId: body.DocumentId,
          password
        });
      } catch (error) {
        throw {
          message:
            "Não foi possível cadastrar o usuário, tente novamente mais tarde",
          status: 500
        };
      }

      user.password = undefined;

      const token = jwt.sign({ id: user.id }, Config.secret, {
        expiresIn: "1 year"
      });

      return {
        message: "Usuário cadastrado com sucesso",
        user,
        status: 200,
        token
      };
    }
  }
});
