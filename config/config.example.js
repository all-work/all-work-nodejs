module.exports = {
  secret: process.env.NODE_SECRET || "your_key",
  development: {
    username: "postgres",
    password: "123456",
    database: "all_work",
    host: "127.0.0.1",
    dialect: "postgres"
  }
};
