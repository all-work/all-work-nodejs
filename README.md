# All Work Node.js Backend

## Before Start

- You need PostgreSQL v10+
- You need Node.js v10+
- You need a database in PostgreSQL called "all_work" (tables will be generated automatically)
- You need to install dependencies
- You need to configure your system

```
cp config/config.example.js config/config.js && nano config/config.js
```

## Install dependencies

```
yarn install # or npm install
```

## Configure for production

```
npm install -g pm2
yarn run prod # or npm run prod
```

## Run Seeders

Run seeders to populate DB (need to run yarn start to create tables first)

```
npx sequelize db:seed:all
```
