// Modules
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const socket = require("socket.io");
const http = require("http");
const models = require("./models");

// Start express
const app = express();

// Config HTTP and Socket
const server = http.Server(app);
const io = socket(server);

app.use((req, res, next) => {
  req.io = io;
  next();
});

// Config server
app.use(cors()); // It allow other server access this data
app.use(bodyParser.json({ limit: "100mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "100mb" }));

// Config routes
app.use("/logs", express.static("logs"));
app.use("/uploads", express.static("uploads"));
app.use("/generated", express.static("generated"));
require("./src/controllers")(app);

// Define port
let port = normalizePort(process.env.NODE_PORT || 8080);
app.set("port", port);

// Change force to reset database (CAUTION)
const force = false;

models.sequelize.sync({ force }).then(() => {
  server.listen(port);
  server.on("error", onError);
  server.on("listening", onListening);
});

function normalizePort(val) {
  let port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

// Listening error events
function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  let bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

// Listening http events
function onListening() {
  let addr = server.address();
  let bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  console.log("Listening on " + bind);
}
