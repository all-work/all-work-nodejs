"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationSchedule = sequelize.define(
    "LocationSchedule",
    {
      dateStart: {
        type: DataTypes.DATEONLY,
        allowNull: false
      },
      dateEnd: {
        type: DataTypes.DATEONLY,
        allowNull: false
      },
      totalValue: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      days: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      paid: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      reversed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      userRating: {
        type: DataTypes.DOUBLE,
        allowNull: true
      },
      locationRating: {
        type: DataTypes.DOUBLE,
        allowNull: true
      }
    },
    {}
  );
  LocationSchedule.associate = function(models) {
    LocationSchedule.belongsTo(models.Location);
    LocationSchedule.belongsTo(models.User);
    LocationSchedule.belongsTo(models.Status);
    LocationSchedule.hasMany(models.LocationScheduleStatusHistory);
  };
  return LocationSchedule;
};
