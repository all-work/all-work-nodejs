"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationScheduleStatusHistory = sequelize.define(
    "LocationScheduleStatusHistory",
    {},
    {}
  );
  LocationScheduleStatusHistory.associate = function(models) {
    LocationScheduleStatusHistory.belongsTo(models.LocationSchedule);
    LocationScheduleStatusHistory.belongsTo(models.Status);
  };
  return LocationScheduleStatusHistory;
};
