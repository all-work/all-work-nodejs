"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationMeetingScheduleStatusHistory = sequelize.define(
    "LocationMeetingScheduleStatusHistory",
    {},
    {}
  );
  LocationMeetingScheduleStatusHistory.associate = function(models) {
    LocationMeetingScheduleStatusHistory.belongsTo(
      models.LocationMeetingSchedule
    );
    LocationMeetingScheduleStatusHistory.belongsTo(models.Status);
  };
  return LocationMeetingScheduleStatusHistory;
};
