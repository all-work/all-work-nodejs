"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationCategory = sequelize.define(
    "LocationCategory",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      }
    },
    {}
  );
  LocationCategory.associate = function(models) {
    LocationCategory.belongsToMany(models.Location, {through: 'LocationCategoryItem'});
  };
  return LocationCategory;
};
