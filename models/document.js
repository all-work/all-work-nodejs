"use strict";
module.exports = (sequelize, DataTypes) => {
  const Document = sequelize.define(
    "Document",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      initials: {
        type: DataTypes.STRING(10),
        allowNull: false
      }
    },
    {}
  );
  Document.associate = function(models) {};
  return Document;
};
