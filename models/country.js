"use strict";
module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define(
    "Country",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      initials: {
        type: DataTypes.STRING(10),
        allowNull: false
      }
    },
    {}
  );
  Country.associate = function(models) {
    Country.hasMany(models.City);
    Country.hasMany(models.State);
  };
  return Country;
};
