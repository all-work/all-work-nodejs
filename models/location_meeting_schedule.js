"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationMeetingSchedule = sequelize.define(
    "LocationMeetingSchedule",
    {
      timeStart: {
        type: DataTypes.TIME,
        allowNull: false
      },
      timeEnd: {
        type: DataTypes.TIME,
        allowNull: false
      },
      totalValue: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      hours: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      paid: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      reversed: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      userRating: {
        type: DataTypes.DOUBLE,
        allowNull: true
      },
      locationRating: {
        type: DataTypes.DOUBLE,
        allowNull: true
      }
    },
    {}
  );
  LocationMeetingSchedule.associate = function(models) {
    LocationMeetingSchedule.belongsTo(models.LocationMeeting);
    LocationMeetingSchedule.belongsTo(models.User);
    LocationMeetingSchedule.belongsTo(models.Status);
    LocationMeetingSchedule.hasMany(
      models.LocationMeetingScheduleStatusHistory
    );
  };
  return LocationMeetingSchedule;
};
