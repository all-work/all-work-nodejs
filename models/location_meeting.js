"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationMeeting = sequelize.define(
    "LocationMeeting",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      hourValue: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      chairs: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      hasTV: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      }
    },
    {}
  );
  LocationMeeting.associate = function(models) {
    LocationMeeting.belongsTo(models.Location);
    LocationMeeting.hasMany(models.LocationMeetingPhoto);
  };
  return LocationMeeting;
};
