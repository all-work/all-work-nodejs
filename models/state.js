"use strict";
module.exports = (sequelize, DataTypes) => {
  const State = sequelize.define(
    "State",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      initials: {
        type: DataTypes.STRING(10),
        allowNull: false
      }
    },
    {}
  );
  State.associate = function(models) {
    State.hasMany(models.City);
    State.belongsTo(models.Country);
  };
  return State;
};
