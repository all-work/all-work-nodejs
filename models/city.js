"use strict";
module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define(
    "City",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      initials: {
        type: DataTypes.STRING(10),
        allowNull: false
      }
    },
    {}
  );
  City.associate = function(models) {
    City.belongsTo(models.State);
    City.belongsTo(models.Country);
  };
  return City;
};
