"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationDays = sequelize.define(
    "LocationDays",
    {
      weekIndex: { // Starts on 0 (Domingo/Sunday)
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: 0
      },
      timeStart: {
        type: DataTypes.TIME,
        allowNull: false
      },
      timeEnd: {
        type: DataTypes.TIME,
        allowNull: false
      }
    },
    {}
  );
  LocationDays.associate = function(models) {
    LocationDays.belongsTo(models.Location);
  };
  return LocationDays;
};
