"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationDocument = sequelize.define(
    "LocationDocument",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      initials: {
        type: DataTypes.STRING(10),
        allowNull: false
      }
    },
    {}
  );
  LocationDocument.associate = function(models) {};
  return LocationDocument;
};
