"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationPhoto = sequelize.define(
    "LocationPhoto",
    {
      photo_url: {
        type: DataTypes.STRING(255),
        allowNull: false
      }
    },
    {}
  );
  LocationPhoto.associate = function(models) {
    LocationPhoto.belongsTo(models.Location);
  };
  return LocationPhoto;
};
