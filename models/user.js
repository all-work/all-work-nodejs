"use strict";
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      name: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      email: {
        type: DataTypes.STRING(255),
        unique: true,
        allowNull: false
      },
      phone: {
        type: DataTypes.STRING(20),
        allowNull: false
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      type: {
        type: DataTypes.ENUM("LOCADOR", "LOCATÁRIO", "ADMINISTRADOR"),
        allowNull: false,
        defaultValue: "LOCADOR"
      },
      password: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      document: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      photo_url: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      zipcode: {
        type: DataTypes.STRING(30),
        allowNull: true
      },
      neighborhood: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      street: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      number: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      complementary: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      latitude: {
        type: DataTypes.DOUBLE,
        allowNull: true
      },
      longitude: {
        type: DataTypes.DOUBLE,
        allowNull: true
      }
    },
    {}
  );
  User.associate = function(models) {
    User.belongsTo(models.Document);
    User.belongsTo(models.City);
    User.belongsTo(models.State);
    User.belongsTo(models.Country);
    User.hasMany(models.Location);
    User.hasMany(models.Notification);
  };
  return User;
};
