"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationPrice = sequelize.define(
    "LocationPrice",
    {
      description: { // Ex: Sala Privada, Sala Compartilhada, ...
        type: DataTypes.STRING(255),
        allowNull: false
      },
      shared: { // Espaco compartilhado (true) ou sala privada (false)
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      valueDay: { // Valor por dia (alugueis de até 30 dias)
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      valueMonth: { // Valor por mes (alugueis > 30 dias) ex: 35 dias = ((valueMonth / 30) * 35)
        type: DataTypes.DOUBLE,
        allowNull: false
      }
    },
    {}
  );
  LocationPrice.associate = function(models) {
    LocationPrice.belongsTo(models.Location);
  };
  return LocationPrice;
};
