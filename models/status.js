"use strict";
module.exports = (sequelize, DataTypes) => {
  const Status = sequelize.define(
    "Status",
    {
      description: {
        type: DataTypes.STRING(255),
        allowNull: false
      }
    },
    {}
  );
  Status.associate = function(models) {};
  return Status;
};
