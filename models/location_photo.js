"use strict";
module.exports = (sequelize, DataTypes) => {
  const LocationMeetingPhoto = sequelize.define(
    "LocationMeetingPhoto",
    {
      photo_url: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      description: {  // Ex: Foto da Sala Privada 01 ...
        type: DataTypes.TEXT,
        allowNull: true
      }
    },
    {}
  );
  LocationMeetingPhoto.associate = function(models) {
    LocationMeetingPhoto.belongsTo(models.LocationMeeting);
  };
  return LocationMeetingPhoto;
};
