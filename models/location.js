"use strict";
module.exports = (sequelize, DataTypes) => {
  const Location = sequelize.define(
    "Location",
    {
      approved: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      company_name: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      document: {
        // In brazil = CNPJ
        type: DataTypes.STRING(100),
        allowNull: false
      },
      document_file_url: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      zipcode: {
        type: DataTypes.STRING(30),
        allowNull: false
      },
      neighborhood: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      street: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      number: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      complementary: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      latitude: {
        type: DataTypes.DOUBLE,
        allowNull: false
      },
      longitude: {
        type: DataTypes.DOUBLE,
        allowNull: false
      }
    },
    {}
  );
  Location.associate = function(models) {
    Location.belongsTo(models.User, {
      as: "Owner"
    });
    Location.belongsTo(models.User, {
      foreignKey: "aprovedBy"
    });
    Location.belongsTo(models.City);
    Location.belongsTo(models.State);
    Location.belongsTo(models.Country);
    Location.belongsTo(models.LocationDocument);
    Location.belongsToMany(models.LocationCategory, {
      through: "LocationCategoryItem"
    });
    Location.hasMany(models.LocationPhoto);
    Location.hasMany(models.LocationPrice);
    Location.hasMany(models.LocationMeeting);
    Location.hasMany(models.LocationDays);
    Location.hasMany(models.LocationSchedule);
  };
  return Location;
};
