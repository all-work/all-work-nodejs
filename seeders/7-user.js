"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          name: "Teste",
          email: "teste@teste.com",
          phone: "5531991969003",
          type: "ADMINISTRADOR",
          password: "$2a$10$cBVMYiGWR/k1Yi8qEpoD2u3uosGbhaiHb5.q0oXDta6MGvl7KssgS",
          document: "12345678901",
          DocumentId: 3,
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  }
};
