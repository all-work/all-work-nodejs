"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "States",
      [
        {
          description: "Minas Gerais",
          initials: "MG",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00",
          CountryId: 1
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("States", null, {});
  }
};
