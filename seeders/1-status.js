"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Statuses",
      [
        {
          description: "Reserved",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "Completed",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "CanceledByUser",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "CanceledByLocation",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "CanceledByAdmin",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Statuses", null, {});
  }
};
