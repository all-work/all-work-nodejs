"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Documents",
      [
        {
          description: "Registro Geral",
          initials: "RG",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "Carteira Nacional de Habilitação",
          initials: "CNH",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "Cadastro de Pessoa Física",
          initials: "CPF",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "Passport",
          initials: "Passport",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Documents", null, {});
  }
};
