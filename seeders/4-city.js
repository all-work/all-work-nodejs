"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Cities",
      [
        {
          description: "Belo Horizonte",
          initials: "BH",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00",
          CountryId: 1,
          StateId: 1
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Cities", null, {});
  }
};
