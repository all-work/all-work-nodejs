"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "LocationCategories",
      [
        {
          description: "FreeCoffee",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "FreeLunch",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "AirConditioning",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        },
        {
          description: "ComputerAccess",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("LocationCategories", null, {});
  }
};
