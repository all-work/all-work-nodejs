"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Countries",
      [
        {
          description: "Brasil",
          initials: "BR",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Countries", null, {});
  }
};
