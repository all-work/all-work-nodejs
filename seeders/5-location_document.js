"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "LocationDocuments",
      [
        {
          description: "Cadastro Nacional da Pessoa Jurídica",
          initials: "CNPJ",
          createdAt: "2019-01-01 00:00:00",
          updatedAt: "2019-01-01 00:00:00"
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("LocationDocuments", null, {});
  }
};
